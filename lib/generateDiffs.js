const fs = require('fs');
const path = require('path');
const async = require('async');

const paths = require('./paths');
const resemble = require('../node_modules/node-resemble-js/resemble.js');
const BlinkDiff = require('blink-diff');

module.exports = function generateDiffs() {
	return new Promise((resolve, reject) => {
		fs.readdir(paths.SCREENSHOTS_TMP, (err, files) => {
			if (err) throw err;

			if (!files) {
				console.error('No files found to diff');
				return;
			}

			const onlyJpegs = files.filter((file) => {
				return file.indexOf('.jpeg') > -1;
			});

			async.eachLimit(onlyJpegs, 1, (filePath, next) => {
				const fullPathForToday = path.resolve(paths.SCREENSHOTS_TMP, filePath);			
				const pathForYesterday = path.resolve(paths.SCREENSHOTS_LATEST, filePath);

				if (fs.existsSync(pathForYesterday) && fs.existsSync(fullPathForToday)) {
/*					var diff = new BlinkDiff({
						imageAPath: pathForYesterday, // Use file-path
						imageBPath: fullPathForToday,
				
						thresholdType: BlinkDiff.THRESHOLD_PERCENT,
						threshold: 0.01, // 1% threshold
				
						imageOutputPath: `${fullPathForToday}-off-by-percent.jpeg`,
				});
				
				diff.run(function (error, result) {
					 if (error) {
							throw error;
					 } else {
							console.log(diff.hasPassed(result.code) ? 'Passed' : 'Failed');
							console.log('Found ' + result.differences + ' differences.');
					 }
				});*/
					resemble(fullPathForToday)
						.compareTo(pathForYesterday)
						.ignoreColors()
						.onComplete((data) => {
							const offBy = data.misMatchPercentage;

							fs.writeFile(`${fullPathForToday}-off-by-${offBy}-percent.jpeg`, fullPathForToday, (err) => {
								if (err) console.error(err);
								console.log(`Diff successfully calculated for ${fullPathForToday}`);
								next();
							});
						});
				} else {
					console.log(`No file exists in latest to diff against ${fullPathForToday}`)
					next();
				}
			}, () => resolve());
		});
	});
}
