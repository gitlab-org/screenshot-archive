const utils = require('./utils');

module.exports = {
  'GitLab Application Tour' : function (browser) {
		// tour the application
		utils.getApplicationTourConfig().reduce((browser, cfg) => {
			return browser[cfg.method](...cfg.args);
		}, browser)
  }
};
