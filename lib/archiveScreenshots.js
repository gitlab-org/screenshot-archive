const shell = require('shelljs');
const paths = require('./paths');

module.exports = function archiveScreenshots() {
  return new Promise((resolve, reject) => {
    const git = paths.GIT_EXECUTABLE;

    shell.exec(`
      cd ${paths.SCREENSHOTS_ARCHIVED}
      NEW_DIR_NAME="$(date +%Y-%m-%d-%T)"
      mkdir $NEW_DIR_NAME
      mv ${paths.SCREENSHOTS_LATEST}/* $NEW_DIR_NAME
      mv ${paths.SCREENSHOTS_TMP}/* ${paths.SCREENSHOTS_LATEST}
    `, { async: true, silent: true }, (err, stdout) => {
        if (err) reject(err);
        resolve();
      });
  });
}

// NEW_DIR_NAME="$(date +%Y-%m-%d-%T)_$(git branch | grep \* | cut -d ' ' -f2)_$(git rev-parse --verify HEAD)"