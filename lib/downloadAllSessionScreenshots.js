const fs = require('fs');
const Promise = require('bluebird');
const axios = require('axios');
const async = require('async');
const https = require('https');
const utils = require('./utils');
const paths = require('./paths');

// TODO: We need to remove 
const apiAuth = {
  auth: {
   username: 'jacobschatz1',
   password: 'dmVmbXvtzvaSqkghzuxn',
  },
 };

module.exports = function downloadAllSessionScreenshots() {
  return fetchBuilds()
    .then(fetchSessions)
    .then(downloadSessionScreenshots)
    .catch((err) => {
      console.error(err);
    });
}

function fetchBuilds() {
  const buildsURL = 'https://www.browserstack.com/automate/builds.json';

  return axios.get(buildsURL, apiAuth)
    .catch((err) => {
      console.error('Error occurred while fetching builds', err);
    });
}

function fetchSessions(response) {
  const buildID = response.data[0].automation_build.hashed_id;
  const buildURL = 'https://www.browserstack.com/automate/builds/<build-id>/sessions.json'.replace('<build-id>', buildID);

  return axios.get(buildURL, apiAuth)
    .catch((err) => {
      console.error('Error occurred while fetching sessions', buildURL, err);
    });
}

function downloadSessionScreenshots(sessions) {
  const sessions_data = sessions.data;
  
  const promises = [];

  if (!sessions_data) return Promise.resolve();

  sessions_data.forEach((session, idx) => {
    const promise = fetchSessionScreenshotLogs(session);
    promises.push(promise);
  });

  return Promise.all(promises);
}

function fetchSessionScreenshotLogs(session_data) {
  const sessionLogsURL = `${session_data.automation_session.logs}.json`;
  const browsername = session_data.automation_session.browser;
  const browserversion = session_data.automation_session.browser_version;

  return axios.get(sessionLogsURL, apiAuth)
    .then(scrapeScreenshotUrls)
    .then(downloadScreenshots.bind(null, browsername, browserversion))
    .catch((err) => {
      console.error('Error occurred while fetching screenshot logs', sessionLogsURL, err);
    });
}

function scrapeScreenshotUrls(response) {
  const awsUrlRegex = /((http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)\w*.jpeg\b/g;

  return response.data.match(awsUrlRegex);
}

function downloadScreenshots(browsername, browserversion, matches) {
  return new Promise((resolve, reject) => {
    let idx = 0;

    if (matches === null) return;

    async.eachLimit(matches, 1, (url, next) => {
      const pageName = utils.getPageNameByIndex(idx);
      const filePath =  `${paths.SCREENSHOTS_TMP}/${browsername}_${browserversion}_${pageName}.jpeg`;

      downloadScreenshot(url, filePath, (err) => {
        if (err) console.error(`An error occurred while downloading the screenshot: ${err}`);
        idx++;
        next();
      });
    }, (err) => {
      console.log(`Screenshots downloaded for ${browsername} ${browserversion}`);
      resolve();
    });

  });
}

function downloadScreenshot(url, dest, cb) {
  const out = fs.createWriteStream(dest);

  out.on('error', (err) => {
    cb(err);
    console.error(`An error occurred while downloading a screenshot to ${dest}`);
  });

  out.on('finish', (err) => {
    cb();
    out.close();      
  });

  https.get(url, (response) => {
    response.pipe(out);
  });
}
