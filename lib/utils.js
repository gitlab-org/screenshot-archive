function getApplicationTourConfig() {
	return [
		{
			method: 'url',
			id: 'sign_in',
			args: ['https://gitlab.com/users/sign_in']
		},
  /*  {
      method: 'setValue',
      id: 'type_username',
      args: ['#user_login', 'fakeymcfakerson'],
    },
    {
      method: 'setValue',
      id: 'type_password',
      args: ['#user_password', 'protein condor choir best'],
    },
    {
      method: 'click',
      id: 'click_login',
      args: ['input[name="commit"]'],
    },
  	{
			method: 'url',
			id: 'project_home',
			args: ['https://gitlab.com/fakeymcfakerson/element'],
		},
		{
			method: 'url',
			id: 'project_issues_index',
			args: ['https://gitlab.com/fakeymcfakerson/element/issues'],
		},
		{
			method: 'url',
			id: 'project_issue',
			args: ['https://gitlab.com/fakeymcfakerson/element/issues/1'],
		},
		{
			method: 'url',
			id: 'project_merge_requests_index',
			args: ['https://gitlab.com/fakeymcfakerson/element/merge_requests/'],
		},
		{
			method: 'url',
			id: 'project_merge_request',
			args: ['https://gitlab.com/fakeymcfakerson/element/merge_requests/1/'],
		},
    {
      method: 'click',
      id: 'click_user_dropdown',
      args: ['.header-user-dropdown-toggle'],
    },
    {
      method: 'click',
      id: 'click_user_dropdown',
      args: ['.sign-out-link'],
    }, */
    {
      method: 'end',
      id: 'end',
      args: [],
    },
	];
}

function getPageNameByIndex(idx) {
	const actions = getApplicationTourConfig();

	return actions[idx].id;	
}

module.exports = {
	getApplicationTourConfig,
	getPageNameByIndex,
}