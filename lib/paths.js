const path = require('path');
const which = require('which');

const CWD = process.cwd();
const NODE_MODULES_PATH = path.resolve(CWD, 'node_modules/');
const SCREENSHOTS_BASE = path.resolve(CWD, 'screenshots/');

module.exports = {
  SCREENSHOTS_BASE,
  SCREENSHOTS_ARCHIVED: path.resolve(SCREENSHOTS_BASE, 'archived/'),
  SCREENSHOTS_LATEST: path.resolve(SCREENSHOTS_BASE, 'latest/'),
  SCREENSHOTS_TMP: path.resolve(SCREENSHOTS_BASE, 'tmp/'),
  BROWSERSTACK_CONFIG: path.resolve(CWD, 'conf/single.conf.js'),
  NIGHTWATCH_ENTRY: path.resolve(CWD, 'lib/test_home.js'),
  NIGHTWATCH_BINARY: path.resolve(NODE_MODULES_PATH, '.bin/nightwatch'),
  GIT_EXECUTABLE: which.sync('git'),
};