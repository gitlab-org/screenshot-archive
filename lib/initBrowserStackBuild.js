const shell = require('shelljs');
const Promise = require('bluebird');
const paths = require('./paths');

module.exports = function initBrowserStackBuild() {
  const BROWSER_ARG = [ 'chrome', 'firefox', 'safari', 'ie'].join(',');
  const CMD = `${paths.NIGHTWATCH_BINARY} -c ${paths.BROWSERSTACK_CONFIG} -e ${BROWSER_ARG} ${paths.NIGHTWATCH_ENTRY}`;

  return new Promise((resolve, reject) => {
    shell.exec(CMD, { async: true, silent: false }, (err, stdout) => {
      if (err) console.error(err, stdout);
      resolve();
    });
  })
}