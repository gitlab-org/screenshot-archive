const shell = require('shelljs');
const paths = require('./paths');

const PREVENT_PUSH = true;

module.exports = function commitAndPushScreenshots() {
  return new Promise((resolve, reject) => {
    if (PREVENT_PUSH) {
      console.warn(`Git push prevented`);
      return resolve();
    }

    const git = paths.GIT_EXECUTABLE;
    const remote = 'origin';
    const branch = 'master';
    const diffCount = 1;
    const commitMessage = `'New screenshots'`;
    // TODO: If there are diff files, count them, and append to commit message
    shell.exec(`
      cd ${paths.SCREENSHOTS_BASE} &&      
      ${git} add . && 
      ${git} commit -m ${commitMessage} && 
      ${git} push ${remote} ${branch}`, { async: true, silent: false }, (err, stdout) => {
        if (err) reject(err);
        resolve(stdout);
      });
  });
}
