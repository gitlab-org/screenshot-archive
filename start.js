const shell = require('shelljs');
const Promise = require('bluebird');

const paths = require('./lib/paths');

const initBrowserStackBuild = require('./lib/initBrowserStackBuild');
const downloadAllSessionScreenshots = require('./lib/downloadAllSessionScreenshots');
const archiveScreenshots = require('./lib/archiveScreenshots');
const generateDiffs = require('./lib/generateDiffs');
const commitAndPushScreenshots = require('./lib/commitAndPushScreenshots');

initBrowserStackBuild()
  .then(downloadAllSessionScreenshots)
  .then(generateDiffs)
  .then(archiveScreenshots)
  .then(commitAndPushScreenshots);