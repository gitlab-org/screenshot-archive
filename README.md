# Overview of the Project

This project is an attempt to catch visual regressions in GitLab sooner with automated browser testing.

The `start` script (run with `yarn start`), performs several tasks in sequence:

1. Initiates headless browser tour of GitLab pages on multiple browsers via BrowserStack, taking screenshots at each action
1. Scrapes semi-public logs from the BrowserStack session logs, finds screenshot URIs, and downloads the screenshots
1. Uses an image diffing library to compare screenshots from the current session from the most recent session (e.g. yesterday) and calculate the difference.
1. Saves the screenshots in the `archive` folder
1. Commits changes and pushes them to a remote repo

# TODOs:

- Remove Jacob's credentials from the repository, and run with env variables.

- Imaging diffing is currently in an in between state (read: doesn't work) -- I played around with a couple of libraries and didn't finish implementing it. Once the diff is successfully generated, the logic for tagging the diff and saving it will properly kick in.

- Get this running against canary, not GitLab.com

- Use a webhook to alert the team when large diffs are present in the newest screenshot set

# Notes:

To whomever takes this over, I got a start on this, after jschatz got a prototype working on his own server. It may take a bit of time to get your bearings, but I believe this is very close to being done. You'll noticed in the `screenshots` dir that it **was** working once!

Imagemagick was the original library of choice for processing screenshots, but it's pretty unwieldy and difficult to drive in JS. There are other JS options that are probably worth exploring.


